# About this fork

This is a fork of Jubaroo's TabardMod. For now, I've added a new one, The Commonwealth. please note that updating to this version if you already had a previous one installed *will* cause issues. Contact me if you want to do that.

# TabardMod

- A mod that allows all kingdom tabards to be crafted and worn by players.

**Version ty1.2.2**
- Fixed name issue

**Version ty1.2.1**
- Added The Commonwealth tabard

**Version 1.2**

- Added the option to toggle the ability for player to craft. Useful to make them GM spawn only for prizes, events, etc.
- Added optional logging for the mod.
- Fixed crafting window issue due to duplicate item ID

**Version 1.1**

- Changed the armor.xml file to reflect the new DDS format textures.
- Added ```depend.requires=serverpacks``` to the properties file to prevent starting server without the serverpack mod installed. Thanks to [ago](https://forum.wurmonline.com/index.php?/profile/827-ago/).
- Added Legion of Anubis tabard. 

![Legion of Anubis Tabard](https://i.imgur.com/VJ7TvzF.png)
