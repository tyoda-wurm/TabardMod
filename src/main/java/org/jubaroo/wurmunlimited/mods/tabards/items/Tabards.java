package org.jubaroo.wurmunlimited.mods.tabards.items;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.IconConstants;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;

import java.io.IOException;
import java.util.logging.Level;

import static org.jubaroo.wurmunlimited.mods.tabards.Initiator.*;


public class Tabards {

    public static void createTabard(String name, String kingdomId, boolean craftable) {
        ItemTemplateBuilder builder = new ItemTemplateBuilder("jubaroo.tabards."+name+" Tabard")
                .name(name+" tabard", "kingdom tabards", "A tabard that is worn to show off which kingdom you belong to.")
                .descriptions("excellent", "good", "ok", "poor")
                .itemTypes(new short[] {
                        ItemTypes.ITEM_TYPE_CLOTH,
                        ItemTypes.ITEM_TYPE_REPAIRABLE
                })
                .behaviourType(BehaviourList.itemBehaviour)
                .imageNumber((short) IconConstants.ICON_ARMOR_TABARD_CLOTH)
                .combatDamage(0)
                .decayTime(3024000L)
                .dimensions(30, 30, 50)
                .primarySkill(-10)
                .bodySpaces(new byte[]{35})
                .modelName("model.mod.clothing.tabard."+kingdomId+'.')
                .difficulty(25.0f)
                .weightGrams(300)
                .material(Materials.MATERIAL_COTTON);
        try {
            ItemTemplate template = builder.build();
            if (craftable) {
                CreationEntryCreator.createSimpleEntry(SkillList.CLOTHTAILORING, ItemList.needleIron,   ItemList.clothYard, template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.CLOTHES);
                CreationEntryCreator.createSimpleEntry(SkillList.CLOTHTAILORING, ItemList.needleCopper, ItemList.clothYard, template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.CLOTHES);
            }
        } catch (IOException e){
            logger.log(Level.SEVERE, "Failed while creating item template for "+name, e);
        }
    }
}
