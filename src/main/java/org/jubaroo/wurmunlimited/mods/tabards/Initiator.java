package org.jubaroo.wurmunlimited.mods.tabards;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.ItemTemplatesCreatedListener;
import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import org.jubaroo.wurmunlimited.mods.tabards.items.Tabards;

import java.util.Properties;
import java.util.logging.Logger;


public class Initiator implements WurmServerMod, ItemTemplatesCreatedListener, Configurable, Versioned {
    public static final String version = "ty1.2.2";
    public static final Logger logger = Logger.getLogger(Initiator.class.getName());
    private boolean tabardCrafting = true;
    private boolean toggleTabards = true;

    public void configure(Properties properties) {
        tabardCrafting = Boolean.parseBoolean(properties.getProperty("tabardCrafting", String.valueOf(tabardCrafting)));
        toggleTabards = Boolean.parseBoolean(properties.getProperty("toggleTabards", String.valueOf(toggleTabards)));
    }

    public void onItemTemplatesCreated() {
        logger.info("Creating tabard items");
        if(toggleTabards) {
            Tabards.createTabard("Jenn-Kellon",          "jenn", tabardCrafting);
            Tabards.createTabard("Jenn-Kellon II",       "zjen", tabardCrafting);
            Tabards.createTabard("Dreadnought",          "drea", tabardCrafting);
            Tabards.createTabard("Pandemonium",          "pand", tabardCrafting);
            Tabards.createTabard("Crusaders",            "thec", tabardCrafting);
            Tabards.createTabard("The Roman Empire",     "ther", tabardCrafting);
            Tabards.createTabard("Horde of the Summoned","hots", tabardCrafting);
            Tabards.createTabard("Valhalla II",          "yval", tabardCrafting);
            Tabards.createTabard("Black Legion",         "blac", tabardCrafting);
            Tabards.createTabard("Ebonaura",             "ebon", tabardCrafting);
            Tabards.createTabard("Abralon",              "abra", tabardCrafting);
            Tabards.createTabard("Mol-Rehan",            "molr", tabardCrafting);
            Tabards.createTabard("Empire of Mol-Rehan",  "empi", tabardCrafting);
            Tabards.createTabard("Kingdom of Sol",       "king", tabardCrafting);
            Tabards.createTabard("Valhalla",             "valh", tabardCrafting);
            Tabards.createTabard("Macedon",              "mace", tabardCrafting);
            Tabards.createTabard("Wurm University",      "wurm", tabardCrafting);
            Tabards.createTabard("Legion of Anubis",     "legi", tabardCrafting);
            Tabards.createTabard("The Commonwealth",     "comm", tabardCrafting);
        }
    }

    @Override
    public String getVersion(){
        return version;
    }
}
